const ShowroomPage = require("../pages-objects/showroomPage"),
  showroompage = new ShowroomPage(),
  CarDetailPage = require("../pages-objects/carDetailPage"),
  cardetailpage = new CarDetailPage(),
  helper = require("../../util/helper"),
  assert = require("assert");
var data = require("../user-data/data.json");

describe("test set - leaseplan web applicaiton", () => {
  afterEach(function () {
    browser.takeScreenshot();
  });

  it("Should open leaseplan web application", () => {
    showroompage.openApplication();
    showroompage.acceptCookie.click();
  });

  it("Filter based on Icon - Select SUV", () => {
    //Filter using Icon Names like "Electric","SUV","Automatic","Hybrid","Petrol","Commercial Vechicle"
    showroompage.iconFilterByName(data.icon.suv).click();
    showroompage
      .selectFilterByName(data.filtername.popularFilters)
      .waitForDisplayed();
    //Verify selected filter in URL
    expect(browser).toHaveUrlContaining(data.icon.suv, { ignoreCase: true });
  });

  it("Filter based on Icon - Select Commercial Vehicle", () => {
    //Filter using Icon Names like "Electric","SUV","Automatic","Hybrid","Petrol","Commercial Vechicle"
    showroompage.iconFilterByName(data.icon.commercial).click();
    var windowHandles = browser.getWindowHandles();
    var window1 = windowHandles[0];
    var window2 = windowHandles[1];
    browser.switchToWindow(window2);
    //Verify selected filter in URL
    expect(browser).toHaveUrlContaining(data.icon.commercial, {
      ignoreCase: true,
    });
  });

  it("Select Filter by Name - Popular Filters->Best deals", () => {
    showroompage.openApplication();
    showroompage.selectFilterByName(data.filtername.popularFilters).click();
    showroompage.filterPopup.waitForDisplayed({ timeout: 5000 });
    assert(showroompage.filterPopup.isDisplayed());
    showroompage.filterPopup.scrollIntoView();
    showroompage
      .selectCheckBoxByName(data.filtername.popularFilterOptions.BestDeals)
      .click();
    showroompage.filterSaveButton.click();
    var convertedText = encodeURIComponent(
      data.filtername.popularFilterOptions.BestDeals.trim()
    );
    expect(browser).toHaveUrlContaining(convertedText, { ignoreCase: true });
  });

  it("Select Filter by Name - FuelTYpe Filters->CNG", () => {
    showroompage.openApplication();
    showroompage.selectFilterByName(data.filtername.fuelTypes).click();
    showroompage.filterPopup.waitForDisplayed({ timeout: 5000 });
    assert(showroompage.filterPopup.isDisplayed());
    showroompage.filterPopup.scrollIntoView();
    showroompage
      .selectCheckBoxByName(data.filtername.fuelTypeFilterOptions.cng)
      .click();
    showroompage.filterSaveButton.click();
    var convertedText = encodeURIComponent(
      data.filtername.fuelTypeFilterOptions.cng.trim()
    );
    expect(browser).toHaveUrlContaining(convertedText, { ignoreCase: true });
  });

  it("Verify total cars count with Filter results - Selection->Audi,Disel", () => {
    //Selecting Audi
    showroompage.openApplication();
    showroompage.selectFilterByName(data.filtername.makemodel).click();
    showroompage.filterPopup.waitForDisplayed({ timeout: 5000 });
    assert(showroompage.filterPopup.isDisplayed());
    showroompage.filterPopup.scrollIntoView();
    showroompage
      .selectCheckBoxByName(data.filtername.makemodelFilterOptions.audi)
      .click();
    showroompage.filterSaveButton.click();
    var convertedText = encodeURIComponent(
      data.filtername.makemodelFilterOptions.audi.trim()
    );
    expect(browser).toHaveUrlContaining(convertedText, { ignoreCase: true });

    //Selecting Disel
    showroompage.selectFilterByName(data.filtername.fuelTypes).click();
    showroompage.filterPopup.waitForDisplayed({ timeout: 5000 });
    assert(showroompage.filterPopup.isDisplayed());
    showroompage.filterPopup.scrollIntoView();
    showroompage
      .selectCheckBoxByName(data.filtername.fuelTypeFilterOptions.diesel)
      .click();
    showroompage.filterSaveButton.click();
    var convertedText = encodeURIComponent(
      data.filtername.fuelTypeFilterOptions.diesel.trim()
    );
    expect(browser).toHaveUrlContaining(convertedText, { ignoreCase: true });

    showroompage.showMoreButton.waitForDisplayed();
    while (showroompage.showMoreButton.isDisplayed()) {
      showroompage.showMoreButton.click();
      browser.pause(4000);
    }
    var totalCards = parseInt(showroompage.carResultsCount.length);
    var content = showroompage.totalCars.getText();
    var totalCars = parseInt(content.toString().split(" to choose")[0]);
    var totalCardWithOption = parseInt(
      showroompage.carResultCountWithChooseOption.length
    );
    console.log("content :" + content);
    console.log("totalCards :" + totalCards);
    console.log("totalCars :" + totalCars);
    var count = 0;
    for (card = 2; card <= totalCards; card++) {
      if (showroompage.invidualCardCount(card).isDisplayed()) {
        var temp = showroompage.invidualCardCount(card).getText();
        var perCardCount = parseInt(temp.toString().split(" to choose")[0]);
        console.log("card : " + card);
        console.log("perCardCount : " + perCardCount);
        count = count + perCardCount;
      }
    }
    var allResultedCars = count + (totalCards - totalCardWithOption);
    console.log(
      "withoutChoose : " + (totalCards - totalCardWithOption).toString()
    );
    console.log("finalCount : " + allResultedCars);
    assert.strictEqual(totalCars, allResultedCars);
  });

  it("Negative Scenario - Verify Empty result content - Selection : Monthly Price,KM", () => {
    browser.url(
      "https://www.leaseplan.com/en-be/business/showroom/?leaseOption[contractDuration]=24&leaseOption[mileage]=10000&monthlyPrice=200,250"
    );
    browser.pause(4000);
    assert(showroompage.sorryMessage.isDisplayed());
  });
});
