class ShowroomPage {
  get acceptCookie() {
    return $(`//button[contains(@class,'accept-cookies-button')]`);
  }

  get resetFilter() {
    return $(`//button[@data-key='resetFilters']`);
  }

  get showMoreButton() {
    return $(`//button[contains(.,'Show more filtered cars')]`);
  }

  get totalCars() {
    return $(`(//span[@data-key='features.showroom.toChooseFrom'])[1]`);
  }

  invidualCardCount(number) {
    return $(`(//span[@data-key='features.showroom.toChooseFrom'])[${number}]`);
  }

  openApplication() {
    return browser.url("https://www.leaseplan.com/en-be/business/showroom/");
  }

  //Icon Names "Electric","SUV","Automatic","Hybrid","Petrol","Commercial Vechicle"
  iconFilterByName(name) {
    return $(`//a[contains(@data-key,'${name}')]`);
  }

  get confirmIconFilterSelection() {
    return $(`//a[@data-tag-id='link-/en-be/business/showroom/']`);
  }

  get carResultsCount() {
    return $$(`//div[@data-component='VehicleGrid']`);
  }

  get carResultCountWithChooseOption() {
    return $$(
      `//div[@data-component='VehicleGrid']//span[@data-key='features.showroom.toChooseFrom']`
    );
  }

  get gallerySwipe() {
    return $(`//div[@class='image-gallery-swipe']`);
  }

  get startLevel() {
    return $(`//div[@style='left: 0%;']`);
  }

  get endLevel() {
    return $(`//div[@style='left: 100%;']`);
  }

  get duration24Monthss() {
    return $(`//div[@class='rc-slider-step']/span[@style='left: 0%;']`);
  }

  selectCardByNumber(number) {
    return $(
      `(//section[@data-tag-id='component-filtered-cars-grid']//a)[${number}]`
    );
  }

  selectFilterByName(name) {
    return $(`(//h3[@data-key='${name}'])[2]`);
  }

  selectCheckBoxByName(name) {
    return $(`//input[@value='${name}']//parent::label`);
  }

  get filterPopup() {
    return $(`//div[@data-key='Inset']`);
  }

  get filterSaveButton() {
    return $(`(//div[@data-key='Inset']/following::div/button)[1]`);
  }

  get leveler() {
    return $(`//div[@class='rc-slider-handle rc-slider-handle-2']`);
  }

  get sorryMessage() {
    return $(`//h3[contains(.,'sorry')]`);
  }
}

module.exports = ShowroomPage;
