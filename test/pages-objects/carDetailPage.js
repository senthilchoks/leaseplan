class CarDetailPage {
  get gallerySwipe() {
    return $(`//div[@class='image-gallery-swipe']`);
  }
}

module.exports = CarDetailPage;
