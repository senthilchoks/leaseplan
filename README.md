# leaseplan

UI E2E Automation Framework using WebdriverIO-Mocha

# install dependencies
npm install or yarn

# run test : 
npm run test or yarn run test

# view report : 
npm run report or yarn run report
