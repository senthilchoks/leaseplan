const fs = require("fs");

class Helper {
  static takeScreenshot() {
    const path = "./allure-results/";
    if (!fs.existsSync(path)) {
      fs.mkdirSync(path, { recursive: true });
    }
    browser.saveScreenshot(path);
  }

  static getRandomNumber(max) {
    const min = 1;
    return (Math.random() * (max - min) + min).toString().split(".")[0];
  }
}

module.exports = Helper;
